using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killable : MonoBehaviour
{
    public GameObject healthBarPrerfab;
    private GameObject healthBar;
    private float health = 100;
    public Transform Target;

    public float RotationSpeed;
    private Quaternion _lookRotation;
    private Vector3 _direction;
    private Quaternion rotation;
    // Start is called before the first frame update
    void Start()
    {
        healthBar = Instantiate(healthBarPrerfab, this.transform.position + new Vector3(0, 1, 0), Camera.main.transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        _direction = (Target.position - transform.position).normalized;
 
         //create the rotation we need to be in to look at the target
         _lookRotation = Quaternion.LookRotation(_direction);
 
         //rotate us over time according to speed until we are in the required rotation

         rotation = Quaternion.Slerp(transform.rotation, _lookRotation, 1000);
         rotation = Quaternion.Euler(0, rotation.eulerAngles.y- 90, -90);
         healthBar.GetComponent<Transform>().position = this.transform.position + new Vector3(0, 1, 0);
         healthBar.GetComponent<Transform>().rotation = rotation;
    }

    void OnCollisionEnter(Collision col){

        health -= 2;
        if(health < 0) {
            Destroy(healthBar);
            Destroy(gameObject);
        }
        healthBar.GetComponent<Transform>().localScale = new Vector3(0.01f, 0, (health/100)*0.3f);
    }
}
