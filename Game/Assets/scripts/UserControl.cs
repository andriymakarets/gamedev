using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserControl : MonoBehaviour
{
    private float speed = 5;
    private float rotationSpeed = 9;
    private bool isGrounded = true;
    public GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        this.transform.Translate(new Vector3(x, 0, z) * Time.deltaTime * speed);      
    }
    void Update()
    {
        this.transform.Rotate(new Vector3(0,Input.GetAxis("Mouse X"), 0) * rotationSpeed);
        Vector3 cameraR = Input.GetAxis("Mouse Y") > 3 || Input.GetAxis("Mouse Y") < -3 ?
            new Vector3(0, 0 , 0):
            new Vector3(Input.GetAxis("Mouse Y"),0 , 0);
        Camera.main.transform.Rotate(-cameraR * 1f);

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            this.GetComponent<Rigidbody>().AddForce(Vector3.up * 6, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            GameObject ball = Instantiate(projectile, this.transform.position, Camera.main.transform.rotation);
            ball.GetComponent<Transform>().position += new Vector3(0, 1, 0);
            ball.GetComponent<Rigidbody>().AddRelativeForce(new Vector3 (0, 100, 600));
            Destroy(ball,6);
        }
    }

    void OnCollisionEnter(Collision col){
        
        isGrounded = true;
        
    }
  
    void OnCollisionExit(Collision col){
        
        isGrounded = false;
        
    }
}
